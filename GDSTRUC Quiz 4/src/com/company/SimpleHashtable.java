package com.company;

import java.util.Collection;

public class SimpleHashtable {
    private Player[] hashtable;
    private int front;
    private int back;
    private Player removedPlayer;
    public string key;
    public string Player;

    public SimpleHashtable()
    {
        hashtable = new Player[10];
    }

    private int hashKey(String key)
    {
        return key.length() % hashtable.length;
    }

    public void put(String key, Player value)
    {
        int hashedKey = hashKey(key);

        if (isOccupied(hashedKey))
        {
            int stoppingIndex = hashedKey;

            if (hashedKey == hashtable.length - 1)
            {
                hashedKey = 0;
            }
            else
            {
                hashedKey ++;
            }

            while (isOccupied(hashedKey) && hashedKey != stoppingIndex)
            {
                hashedKey = (hashedKey + 1) % hashtable.length;
            }
        }

        if (isOccupied(hashedKey))
        {
            System.out.println("There is already and element at position " + hashedKey);
        }
        else
        {
            hashtable[hashedKey] = value;
        }
    }

    public Player get(String key)
    {
        int hashedKey = hashKey(key);

        if (hashedKey == -1)
        {
            return null;
        }
        System.out.println("BAQSBAQSBAQS: " + hashedKey);
        return hashtable[hashedKey];
    }

    public void printHashtable()
    {
        for (int i = 0; i < hashtable.length; i++)
        {
            System.out.println("Element " + i + " " + hashtable[i]);
        }
    }

    private  boolean isOccupied(int index)
    {
        return hashtable [index] != null;
    }

    public void remove(String key)
    {
        int hashedKey = hashKey(key);

        hashtable[hashedKey] = null;
    }
}
