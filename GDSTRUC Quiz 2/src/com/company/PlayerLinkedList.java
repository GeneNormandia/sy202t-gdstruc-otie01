package com.company;

public class PlayerLinkedList {
    private PlayerNode head;

    public void printList() {
        PlayerNode current = head;
        System.out.print("Printing list: \n" + "HEAD -> ");
        while (current != null) {
            System.out.print(current.getPlayer().getName());
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }

    public void addToFront(Player player) {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }

    public void deleteFromFirst() {
        head = head.getNextPlayer();
    }

    public void getSize() {
        int size = 0;
        PlayerNode current = head;

        while (current != null) {
            size++;
            current = current.getNextPlayer();
        }

        System.out.println("The size of the array is: " + size);
    }

    public void contains(String name) {
        boolean isContained = false;
        PlayerNode current = head;
        while (current != null && !isContained)
        {
            if (current.getPlayer().getName().equals(name))
            {
                isContained = true;
                System.out.println("Is" + name + " in the list: " + isContained);
            }

            if (!current.getPlayer().getName().equals(name))
            {
                current = current.getNextPlayer();
            }
        }

        if (current == null) {
            isContained = false;
            System.out.println("Is " + name + " in the list: " + isContained);
        }
    }

    public void indexOf(String name) {
        int index = 0;
        boolean isFound = false;
        PlayerNode current = head;
        while (current != null && !isFound)
        {
            if (!current.getPlayer().getName().equals(name))
            {
                current = current.getNextPlayer();
                index++;
            }

            if (current.getPlayer().getName().equals(name))
            {
                System.out.println(name + " is at index: " + index);
                isFound = true;
            }
        }

        if (current == null)
        {
            System.out.println("Object does not exist.");
        }
    }

}
