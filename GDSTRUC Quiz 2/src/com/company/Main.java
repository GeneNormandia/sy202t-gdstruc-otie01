package com.company;

public class Main {

    public static void main(String[] args) {

        Player drone = new Player("Drone");
        Player wardell = new Player( "Wardell");
        Player subroza = new Player( "Subroza");

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        //Populate the linked list
        playerLinkedList.addToFront(drone);
        playerLinkedList.addToFront(wardell);
        playerLinkedList.addToFront(subroza);
        //Print the list
        playerLinkedList.printList();
        //Check if these elements exist in the list
        playerLinkedList.contains("Drone");
        playerLinkedList.contains("Cutler");
        playerLinkedList.contains("Hazed");
        //Display what is the index of the element
        playerLinkedList.indexOf("Drone");
        playerLinkedList.indexOf("Wardell");
        playerLinkedList.indexOf("Subroza");
        //Display the size of the list
        playerLinkedList.getSize();

        //Remove the head of the list
        System.out.println("\n" + "Deleting the first element from the list...");
        playerLinkedList.deleteFromFirst();
        //Print the list
        playerLinkedList.printList();
        //Check if these elements exist in the list
        playerLinkedList.contains("Subroza");
        //Display what is the index of the element
        playerLinkedList.indexOf("Drone");
        playerLinkedList.indexOf("Wardell");
        //Display the size of the list
        playerLinkedList.getSize();


     }
}
