package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Tree tree = new Tree();

        tree.insert(10);
        tree.insert(40);
        tree.insert(15);
        tree.insert(20);
        tree.insert(5);
        tree.insert(25);
        tree.insert(30);
        tree.insert(22);
        tree.insert(2);

       //Sort the elements in descending order
       tree.traverseInDescendingOrder();
        //Display the minimum value
        System.out.println("Minimum value is: " + tree.getMin());
        //Display the maximum value
        System.out.println("Maximum value is: " + tree.getMax());

    }
}
