package com.company;

public class Node {
    private int data;
    private Node rightChild;
    private Node leftChild;

    public void insert(int value)
    {
        if(value == data)
        {
            return;
        }

        if(value < data)
        {
            if (leftChild == null)
            {
                leftChild = new Node(value);
            }
            else
            {
                leftChild.insert(value);
            }
        }
        else
        {
            if(rightChild == null)
            {
                rightChild = new Node(value);
            }
            else
            {
                rightChild.insert(value);
            }
        }
    }

    public void traverseInDescendingOrder()
    {
        if(rightChild != null)
        {
            rightChild.traverseInDescendingOrder();
        }
        System.out.println("Data in descending order " + data);
        if(leftChild != null)
        {
            leftChild.traverseInDescendingOrder();
        }
    }

    public Node get(int value)
    {
        if(value == data)
        {
            return this;
        }
        if(value < data)
        {
            return leftChild.get(value);
        }
        else
        {
            if(rightChild != null)
            {
                return rightChild.get(value);
            }
        }
        return null;
    }

    public Node getMin()
    {
            if (this.leftChild == null)
            {
                return this;
            }
        return leftChild.getMin();
    }

    public Node getMax()
    {
            if (this.rightChild == null)
            {
                return this;
            }
            return rightChild.getMax();
    }

    public Node (int _data)
    {
        this.data = _data;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Node getRightChild() {
        return rightChild;
    }

    public void setRightChild(Node rightChild) {
        this.rightChild = rightChild;
    }

    public Node getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(Node leftChild) {
        this.leftChild = leftChild;
    }

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                '}';
    }
}
