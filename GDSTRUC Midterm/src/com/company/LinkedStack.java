package com.company;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;

public class LinkedStack {

    private LinkedList<Card> stack;
    private int data;
    private LinkedStack next;

    public LinkedStack()
    {
        stack = new LinkedList<Card>();
    }
    public void push(Card card)
    {
        stack.push(card);
    }

    public boolean isEmpty()
    {
        return  stack.isEmpty();
    }

    public Card pop()
    {
        return stack.pop();
    }

    public Card peek()
    {
        return stack.peek();
    }

    public void printStack()
    {
        ListIterator<Card> iterator = stack.listIterator();

        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }

    public void userInput()
    {
        System.out.println("Input a text: ");
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        System.out.println("You have entered; " + input);
    }


    public class Node {

        int data;

        Node next;

    }

    public Node head;
    public Node tail;
    public int size;

    public int getFirst() throws Exception {

        if (this.size == 0) {

            throw new Exception("linked list is empty");
        }

        return this.head.data;
    }

    public int getLast() throws Exception {

        if (this.size == 0) {

            throw new Exception("linked list is empty");
        }
        return this.tail.data;
    }

    public void display() {

        Node temp = this.head;
        while (temp != null) {
            System.out.println(temp.data + " ");
            temp = temp.next;
        }
    }

    public void addFirst(int item) {

        Node nn = new Node();

        nn.data = item;
        if (this.size == 0) {
            this.head = nn;
            this.tail = nn;
            this.size = this.size + 1;

        } else {

            nn.next = this.head;

            this.head = nn;

            this.size = this.size + 1;

        }

    }





}
