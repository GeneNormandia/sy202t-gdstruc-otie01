package com.company;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;

public class PlayerStack {

    private LinkedList<Card> stack2;

    public PlayerStack()
    {
        stack2 = new LinkedList<Card>();
    }
    public void push(Card card)
    {
        stack2.push(card);
    }

    public boolean isEmpty()
    {
        return  stack2.isEmpty();
    }

    public Card pop()
    {
        return stack2.pop();
    }

    public Card peek()
    {
        return stack2.peek();
    }

    public void printStack()
    {
        ListIterator<Card> iterator = stack2.listIterator();
        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }




}
