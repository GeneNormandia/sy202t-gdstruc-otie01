package com.company;

import javafx.scene.Node;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;

public class DiscardedStack {

    private LinkedList<Card> stack3;

    public DiscardedStack()
    {
        stack3 = new LinkedList<Card>();
    }
    public void push(Card card)
    {
        stack3.push(card);
    }

    public boolean isEmpty()
    {
        return  stack3.isEmpty();
    }

    public Card pop()
    {
        return stack3.pop();
    }

    public Card peek()
    {
        return stack3.peek();
    }

    public void printStack()
    {
        ListIterator<Card> iterator = stack3.listIterator();
        System.out.println("Printing stack");
        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }




}
