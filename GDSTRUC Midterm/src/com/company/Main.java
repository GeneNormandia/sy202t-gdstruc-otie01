package com.company;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        LinkedStack stack = new LinkedStack();
        PlayerStack stack2 = new PlayerStack();
        DiscardedStack stack3 = new DiscardedStack();

        //Populate the deck of cards
        stack.push(new Card(1, "Red"));
        stack.push(new Card(2, "Red"));
        stack.push(new Card(3, "Red"));
        stack.push(new Card(4, "Red"));
        stack.push(new Card(5, "Red"));
        stack.push(new Card(6, "Red"));
        stack.push(new Card(7, "Red"));
        stack.push(new Card(8, "Red"));
        stack.push(new Card(9, "Red"));
        stack.push(new Card(10, "Red"));
        stack.push(new Card(11, "Red"));
        stack.push(new Card(12, "Red"));
        stack.push(new Card(13, "Red"));
        stack.push(new Card(14, "Red"));
        stack.push(new Card(15, "Red"));
        stack.push(new Card(1, "Blue"));
        stack.push(new Card(2, "Blue"));
        stack.push(new Card(3, "Blue"));
        stack.push(new Card(4, "Blue"));
        stack.push(new Card(5, "Blue"));
        stack.push(new Card(6, "Blue"));
        stack.push(new Card(7, "Blue"));
        stack.push(new Card(8, "Blue"));
        stack.push(new Card(9, "Blue"));
        stack.push(new Card(10, "Blue"));
        stack.push(new Card(11, "Blue"));
        stack.push(new Card(12, "Blue"));
        stack.push(new Card(13, "Blue"));
        stack.push(new Card(14, "Blue"));
        stack.push(new Card(15, "Blue"));

        int sizeOfHand;
        sizeOfHand = 0;
        int sizeOfDeck;
        sizeOfDeck = 30;
        int sizeOfDiscarded = 0;

        while (sizeOfDeck != 0)
        {
            //Get input from user
            System.out.println("Press enter to continue: ");
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();

            //Generate a random command every turn
            Random command = new Random();
            int number = 0;

            number = 1 + command.nextInt(3);
            //At the start of the game, the player gets to draw first
            if (sizeOfHand == 0)
            {
                number = number = 1;
            }

            //Randomizer of how many cards the player draws or discards
            Random card = new Random();
            int numberOfCards;
            numberOfCards = 1 + card.nextInt(5);

            //Player draws cards from the deck
            if (number == 1)
            {
                System.out.println("Player draws!");
                if (sizeOfDeck < numberOfCards)
                {
                    numberOfCards = numberOfCards = sizeOfDeck;
                }
                    System.out.println("Player draws " + numberOfCards + " cards from the deck!");
                    sizeOfDeck = sizeOfDeck - numberOfCards;
                    sizeOfHand = sizeOfHand + numberOfCards;
                    for (int i = numberOfCards - 1; i >= 0; i--) {
                        stack2.push(stack.pop());
                    }
            }

            //Player discards from hand
            if (number == 2) {
                System.out.println("Player discards!");
                if (sizeOfHand == 0)
                {
                    System.out.println("Not enough cards in hand. Roll again!");
                    numberOfCards = numberOfCards = 0;
                }
                if (sizeOfHand < numberOfCards)
                {
                    numberOfCards = numberOfCards = sizeOfHand;
                }

                if (sizeOfHand >= numberOfCards) {
                    System.out.println("Player discards " + numberOfCards + " cards from their hand!");
                    sizeOfHand = sizeOfHand - numberOfCards;
                    sizeOfDiscarded = sizeOfDiscarded + numberOfCards;
                    for (int i = numberOfCards - 1; i >= 0; i--) {
                        stack3.push(stack2.pop());
                    }
                }
            }

            //Player draws from the discarded pile
            if (number == 3) {
                System.out.println("Player draws from discarded pile!");
                if (sizeOfDiscarded == 0)
                {
                    System.out.println("There are not enough discarded cards. Roll again!");
                }
                if (sizeOfDiscarded < numberOfCards)
                {
                    numberOfCards = numberOfCards = sizeOfDiscarded;
                }
                if (sizeOfDiscarded >= numberOfCards) {
                    System.out.println("Player draws " + numberOfCards + " cards from the discarded deck!");
                    sizeOfDiscarded = sizeOfDiscarded - numberOfCards;
                    for (int i = numberOfCards - 1; i >= 0; i--) {
                        stack2.push(stack3.pop());
                    }
                }
            }

            //Print the hand of the player
            System.out.println("Number of player's cards: " + sizeOfHand + "\n" + "Player's hand: ");
            stack2.printStack();

            //Print the size of the deck and the discarded cards
            System.out.println("Remaining cards on the deck: " + sizeOfDeck);
            System.out.println("Cards on the discarded pile: " + sizeOfDiscarded);
        }
        //Game ends when there are no more cards left in the deck
        System.out.println("Game Over! No more cards left in the deck!");
    }
}

