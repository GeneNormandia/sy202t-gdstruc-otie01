package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        ArrayQueue queue = new ArrayQueue(5);

        queue.add(new Player(1, "Drone",100));
        queue.add(new Player(2, "Subroza",100));
        queue.add(new Player(3, "Wardell",95));
        queue.add(new Player(4, "Hazed",90));
        queue.add(new Player(5, "Cutler",90));
        queue.add(new Player(6, "FNS", 90));
        queue.add(new Player(7, "Food", 95));
        queue.add(new Player(8, "Crashies", 90));
        queue.add(new Player(9, "Kaboose", 90));
        queue.add(new Player(10, "Mummay", 95));
        queue.add(new Player(11, "Tenz", 100));
        queue.add(new Player(12, "Shahzam", 90));
        queue.add(new Player(13, "Darp", 90));
        queue.add(new Player(14, "Sick", 95));
        queue.add(new Player(15, "Zombs", 90));
        queue.add(new Player(16, "Zacharee", 90));
        queue.add(new Player(17, "Marved", 90));
        queue.add(new Player(18, "Rawkus", 90));
        queue.add(new Player(19, "Corey", 90));
        queue.add(new Player(20, "Babybay", 95));
        queue.add(new Player(21, "Steel", 90));
        queue.add(new Player(22, "Hiko", 100));
        queue.add(new Player(23, "Nitr0", 90));
        queue.add(new Player(24, "Asuna", 90));
        queue.add(new Player(25, "Dicey", 90));
        queue.add(new Player(26, "Ethan", 90));
        queue.add(new Player(27, "YaBoiDre", 95));
        queue.add(new Player(28, "Moose", 90));
        queue.add(new Player(29, "Aproto", 90));
        queue.add(new Player(30, "Stellar", 90));
        queue.add(new Player(31, "Leaf", 95));
        queue.add(new Player(32, "Nature", 90));
        queue.add(new Player(33, "Kehmicals", 90));
        queue.add(new Player(34, "ShotUp", 90));
        queue.add(new Player(35, "JcStani", 90));
        queue.add(new Player(36, "Genghsta", 90));
        queue.add(new Player(37, "Hyunh", 95));
        queue.add(new Player(38, "Mikael", 90));
        queue.add(new Player(39, "GMD", 90));
        queue.add(new Player(40, "Koosta", 90));
        queue.add(new Player(41, "Eeiu", 90));
        queue.add(new Player(42, "Shawn", 95));
        queue.add(new Player(43, "Xeta", 90));
        queue.add(new Player(44, "Vice", 90));
        queue.add(new Player(45, "Relyks", 90));
        queue.add(new Player(46, "Leaf", 90));
        queue.add(new Player(47, "Mich", 90));
        queue.add(new Player(48, "Pois", 90));
        queue.add(new Player(49, "PureR", 90));
        queue.add(new Player(50, "Ayrin", 90));

        int games = 0;

        while (games < 10)
        {
            Random command = new Random();
            int players;
            players = 1 + command.nextInt(7);

            System.out.println("On going games: " + games+ "\nPlayers in queue: " + players + "\nFirst player in queue: " + queue.peek());

              if (players < 4)
              {
                  System.out.println("Not enough players to start a match!");
                  System.out.println("Press enter to continue: ");
                  Scanner scanner = new Scanner(System.in);
                   String input = scanner.nextLine();
              }
              if (players >= 5)
              {
                  System.out.println("Enough players to start a match!\nStarting match...");
                  System.out.println("Press enter to continue: ");
                  for (int i = 5 - 1; i >= 0; i--)
                  {
                      queue.remove();
                  }
                  Scanner scanner = new Scanner(System.in);
                  String input = scanner.nextLine();
                  games = games + 1;
              }
        }
        System.out.println("Current games: " + games + "\nServer is full");
    }
}
