package com.company;

public class Main {

    public static void main(String[] args) {

        int[] numbers = new int[10];
        numbers[0] = 8;
        numbers[1] = 6;
        numbers[2] = 5;
        numbers[3] = 4;
        numbers[4] = 1;
        numbers[5] = 10;
        numbers[6] = 3;
        numbers[7] = 9;
        numbers[8] = 7;
        numbers[9] = 2;

        //Prints the contents of the array
        System.out.println("Before sorting");
        printArrayElements(numbers);

        //Bubble sorts the contents of the array in descending order
        bubbleSort(numbers);
        System.out.println("\n\nAfter bubble sort sort in descending order");
        printArrayElements(numbers);

        //Sorts the array in descending order by looking for the smallest value first and putting it at the end
        selectionSort(numbers);
        System.out.println("\n\nAfter selection sort in descending order");
        printArrayElements(numbers);
    }

    private static void bubbleSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for (int i = 0; i < lastSortedIndex; i++)
            {
                if (arr[i] < arr[i + 1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
        }
    }

    private static void selectionSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            int smallestIndex = 0;
            for (int i = 1; i <= lastSortedIndex; i++)
            {
                if (arr[i] < arr[smallestIndex])
                {
                    smallestIndex = i;
                }
            }
            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[smallestIndex];
            arr[smallestIndex] = temp;
        }
    }

    private static void printArrayElements(int[] arr)
    {
        for(int j : arr)
        {
            System.out.print(j + " ");
        }
    }

}












